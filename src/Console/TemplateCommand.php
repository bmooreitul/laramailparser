<?php 
namespace App\Console\Commands;

use Illuminate\Console\Command;
use ZBateson\MailMimeParser\MailMimeParser;
use Illuminate\Filesystem\Filesystem;
use Itul\Laramailparser\LaramailDefaults;

class TemplateCommand extends Command
{
    use LaramailDefaults;

    //PROPERTIES THAT CAN BE MODIFIED
    protected $storeRaw         = true;         //TOGGLE STORING RAW INBOUND EMAILS TO THE DATABASE BEFORE PARSING (USEFUL FOR DEBUGGING)
    protected $storeRawTable    = 'raw_emails'; //THE TABLE NAME TO STORE RAW EMAILS TO (WILL BE AUTO CREATED IF NEEDED. IS IGNORED WHEN $storeRaw = false)

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "laramailparser-parse {--debug=false : The name of the raw email. This will process a previously stored raw email instead of an incoming one}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse Raw Emails Piped To The Application';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handled(){
        /*
            You have the following variables already parsed for you:

            $this->fromName             //THE NAME OF THE PERSON THAT SENT THIS EMAIL
            $this->fromEmail            //THE EMAIL ADDRESS THIS EMAIL WAS SENT FROM
            $this->toName               //THE NAME OF THE PERSON THAT THIS EMAIL WAS SENT TO
            $this->toEmail              //THE EMAIL ADDRESS THAT THIS EMAIL WAS SENT TO
            $this->ccs                  //AN ARRAY OF EMAILS CC'D ON THIS EMAIL
            $this->subject              //THE SUBJECT LINE OF THIS EMAIL
            $this->content              //THE CONTENT OF THIS EMAIL FORMATTED AS HTML
            $this->attachments          //AN ARRAY OF ATTACHMENTS FOR THIS EMAIL {name,extension,tempname,mime,size}
            $this->storedEmailId        //THE ID OF THE DATABASE RECORD FOR THE RAW VERSION OF THIS EMAIL (ONLY IF $this->storeRaw is TRUE)
            $this->storedRawEmailPath   //THIS IS THE PATH OF THE STORED EMAIL (RAW CONTENT WRITTEN TO A FILE)
            $this->mailParser           //THIS IS THE PARSING CLASS. FOR MORE INFORMATION ON THIS CLASS VISIT https://github.com/zbateson/mail-mime-parser

            WRITE YOUR CODE BELOW
        */
    }
}