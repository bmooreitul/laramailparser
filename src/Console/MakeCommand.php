<?php

namespace Itul\Laramailparser\Console;

use Illuminate\Console\Command;
use Itul\Laramailparser\Laramailparser;

class MakeCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laramailparser:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a new laramailparser command';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        

        $name = $this->ask("What name do you want to use for this parser?");
        $dir = $this->laravel->path('Console/Commands');

        if(!file_exists($dir)) mkdir($dir, 0755, true);

        if(!file_exists($dir.'/'.$name.'.php')){
            $data = file_get_contents(__DIR__.'/TemplateCommand.php');

            $data = str_replace(['class TemplateCommand', 'protected $signature = "laramailparser-parse'], ['class '.$name, 'protected $signature = "laramailparser-'.str_replace('command', '', strtolower($name))], $data);
            file_put_contents($dir.'/'.$name.'.php', $data);

             $this->info('New Parser Command Created!');
        }
        else{
            $this->error('Failed to create command');
            return false;
        }
    }
}