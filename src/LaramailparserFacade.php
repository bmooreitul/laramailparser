<?php

namespace Itul\Laramailparser;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Itul\Laramailparser\Skeleton\SkeletonClass
 */
class LaramailparserFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'laramailparser';
    }
}
