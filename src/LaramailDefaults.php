<?php

namespace Itul\Laramailparser;
use ZBateson\MailMimeParser\MailMimeParser;

trait LaramailDefaults {

	public function isCli(){
		return (php_sapi_name() === 'cli');
	}

	public function handle(){
		$this->parseDefaults();
		return $this->handled();
	}

	public function parseDefaults(){

		//DEFINE THE DEFAULTS
		$defaults = [
			'storeRaw' 				=> true,
			'storeRawTable' 		=> 'raw_emails',
			'debug' 				=> false,
			'fromName' 				=> null,
			'toName' 				=> null,
			'toEmail' 				=> null,
			'ccs' 					=> [],
			'subject' 				=> null,
			'content' 				=> null,
			'attachments' 			=> [],
			'storedEmailId' 		=> null,
			'storedRawEmailPath' 	=> null,
			'mailParser' 			=> null,
		];

		//SET DEFAULT VALUES THAT ARENT DEFINED
		foreach($defaults as $k => $defaultVal) if(!property_exists($this, $k)) $this->$k = $defaultVal;

		//DEFINE THE LOCATION FOR RAW EMAILS
		$raw_email_dir = $this->storeRaw ? storage_path('private/email/raw') : sys_get_temp_dir(); 

		//CHECK FOR DEBUG OPTION
		$debug          = $this->option('debug');
		$this->debug    = $debug == 'false'  ? false : $debug;

		if(!$this->debug){

			//GET THE CONTENT STREAM
			$raw_email_content  = "";
			$fd                 = fopen("php://stdin", "r");        
			while(!feof($fd)) $raw_email_content .= fread($fd, 1024);
			fclose($fd);

			//CHECK IF WE WANT TO STORE THE RAW EMAIL
			if($this->storeRaw){

				//CHECK IF THE RAW EMAIL TABLE EXISTS
				try{
					$dbName     = \Config::get('database.connections.'.\Config::get('database.default').'.database');
					$tableName  = "`{$dbName}`.`{$this->storeRawTable}`";
					$res        = \DB::select(\DB::raw("SELECT 1 FROM {$tableName} LIMIT 1;"));
				}

				//CREATE THE RAW TABLE
				catch(\Exception $e){      
					$res = \DB::select(\DB::raw("
						CREATE TABLE {$tableName} (
							`id` INT NOT NULL AUTO_INCREMENT , 
							`from` VARCHAR(255) NOT NULL , 
							`subject` VARCHAR(255) NULL DEFAULT NULL , 
							`path` VARCHAR(255) NULL DEFAULT NULL , 
							`name` VARCHAR(255) NULL DEFAULT NULL , 
							`size` BIGINT(30) NOT NULL DEFAULT '0' , 
							PRIMARY KEY (`id`)
						) ENGINE = MyISAM;
					"));
				}

				//CREATE THE DIRECTORY IF NEEDED
				if(!file_exists($raw_email_dir)) mkdir($raw_email_dir, 0755, true);
			}

			try{
				 //CREATE A UNIQUE FILE
				$raw_email_path     = tempnam($raw_email_dir, 'raw_email_');
			}
			catch(\Exception $e){
				abort(500);
				return;
			}
			
			//PUT THE CONTENT OF THE RAW EMAIL INTO THE TEMP FILE
			file_put_contents($raw_email_path, $raw_email_content);

			//SET THE STORED RAW EMAIL PATH
			$this->storedRawEmailPath = $raw_email_path;
		}
		else{

			//DISABLE STORAGE IN DEBUG MODE
			$this->storeRaw     = false;

			//GET THE RAW EMAIL CONTENT
			$raw_email_content  = file_get_contents($raw_email_dir.'/'.$this->debug);

		}

		//PARSE THE INCOMING EMAIL
		$this->mailParser   = new MailMimeParser();
		$this->message      = $this->mailParser->parse($raw_email_content, false);     

		//SET DEFAULTS
		$from_name          = $this->message->getHeader('from')->getPersonName();
		$this->fromEmail    = $this->message->getHeaderValue('from');
		$this->fromName     = $from_name == $this->fromEmail ? '' : $from_name;        
		$this->toEmail      = $this->message->getHeaderValue('to');
		$to_name            = $this->message->getHeader('to')->getPersonName();
		$this->toName       = $to_name == $this->toEmail ? '' : $to_name;
		$this->subject      = $this->message->getHeaderValue('subject');
		

		//CHECK IF WE WANT TO STORE THE RAW EMAIL
		if($this->storeRaw){
			
			//INSERT THE RAW EMAIL INTO THE DATABASE
			$this->storedEmailId = \DB::table($this->storeRawTable)->insertGetId([
				'from'      => $this->fromEmail,
				'subject'   => $this->subject,
				'path'      => $raw_email_dir,
				'name'      => basename($raw_email_path),
				'size'      => filesize($raw_email_path),
			]);
		}

		//PARSE THE CONTENT
		$this->parseContent();

		//PARSE THE CC FIELD
		$this->parseCCS();

		//PARSE THE ATTACHMENTS
		$this->parseAttachments();

		//PRINT OUT THE PARSED DATA
		if($this->isCli() && $this->debug){
            $this->info('From Name: '.$this->fromName);
            $this->info('From Email: '.$this->fromEmail);
            $this->info('To Name: '.$this->toName);
            $this->info('To Email: '.$this->toEmail);
            $this->info('Subject: '.$this->subject);
            $this->info('CC: '.implode(', ',$this->ccs));
            $this->info('Attachment Total: '.count($this->attachments));
            $this->info('Content: '.$this->content);
            ob_start();
            $this->attachments ? print_r($this->attachments) : var_dump($this->attachments);
            $attachmentPrint = ob_get_clean();
            $this->info('Attachments: '.$attachmentPrint);
        }
	}

	public function parseCCS(){

		//BUILD THE CCS
		if($ccHeader        = $this->message->getHeader('cc')) foreach($ccHeader->getAddresses() as $address) $this->ccs[] =  $address->getEmail();

		return $this->ccs;
	}

	public function parseAttachments(){

		//CHECK FOR ATTACHMENTS
		if($this->message->getAttachmentCount()){

			//LOOP THROUGH THE ATTACHMENTS
			foreach($this->message->getAllAttachmentParts() as $attachment){

				//GET DATA
				$content        = $attachment->getContent();
				$name           = $attachment->getHeader('content-disposition')->getValueFor('filename');
				$path 			= tempnam(sys_get_temp_dir(), 'email_attachment_');

				//CREATE THE TEMP FILE
				file_put_contents($path, $content);				

				//ADD TO THE ATTACHMENTS ARRAY
				$this->attachments[] = (object)[
					'name' 		=> $name,
					'extension' => pathinfo($name, PATHINFO_EXTENSION),
					'tempname' 	=> $path,
					'mime' 		=> $attachment->getHeaderValue('content-type'),
					'size' 		=> filesize($path),
				];
			}
		}

		return $this->attachments;
	}

	public function parseContent(){        

		if($this->message->getHtmlPartCount()){    
			$content = (new \HTMLPurifier(\HTMLPurifier_Config::createDefault()))->purify($this->message->getHtmlContent());
			$content = str_replace([' class="MsoNormal"',' class="WordSection1"'], '', $content);
		}
		else{
			$content = nl2br(trim($message->getTextContent()));
		}  

		return $this->content = str_replace(['<p></p>', '<p> </p>'], '', $content);
	}
}