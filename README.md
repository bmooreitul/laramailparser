# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/itul/laramailparser.svg?style=flat-square)](https://packagist.org/packages/itul/laramailparser)
[![Build Status](https://img.shields.io/travis/itul/laramailparser/master.svg?style=flat-square)](https://travis-ci.org/itul/laramailparser)
[![Quality Score](https://img.shields.io/scrutinizer/g/itul/laramailparser.svg?style=flat-square)](https://scrutinizer-ci.com/g/itul/laramailparser)
[![Total Downloads](https://img.shields.io/packagist/dt/itul/laramailparser.svg?style=flat-square)](https://packagist.org/packages/itul/laramailparser)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require itul/laramailparser
```

Make a new parser from a template
```bash
php artisan laramailparser:make
```

This will ask you for the name of the new parser.

Once you input the name it will be generated under app/Console/Commands

Now you are free to edit the new command you just made.


## Routing
Once you have the parser setup you will need to route inbound email to it.

This can be done easilly from cpanel by setting up a `Default Address` under the mail section.

In the Default Address Screen
 - select the domain you want to use.
 - Under `Advanced Options`: choose `Pipe to a Program`
 - In the path input enter `php -q /PATH/TO/LARAVEL/artisan NAMEOFPARSERHERE`
 - Replace `/PATH/TO/LARAVEL` with the actual path to your project
 - Replace `NAMEOFPARSERHERE` with the name of the parser you created